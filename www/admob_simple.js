
var admobid = {};

if( /(android)/i.test(navigator.userAgent) ) {
  admobid = { // for Android
    banner: 'ca-app-pub-4164704583128754~6541913770',
    interstitial: 'ca-app-pub-4164704583128754/5037260416'
  };
} else if(/(ipod|iphone|ipad)/i.test(navigator.userAgent)) {
  admobid = { // for iOS
    banner: 'ca-app-pub-4164704583128754~6541913770',
    interstitial: 'ca-app-pub-4164704583128754/5037260416'
  };
} else {
  admobid = { // for Windows Phone
    banner: 'ca-app-pub-4164704583128754~6541913770',
    interstitial: 'ca-app-pub-4164704583128754/5037260416'
  };
}

function advads() {

  if (! AdMob ) { alert( 'admob plugin not ready' ); return; }
	
 
  AdMob.createBanner( {
    adId: admobid.banner,
	
    position: AdMob.AD_POSITION.BOTTOM_CENTER,
    adSize: AdMob.AD_SIZE.SMART_BANNER,
    overlap: true,
    offsetTopBar: true,
    bgColor: 'black'
  } );

  // this will load a full screen ad on startup
  AdMob.prepareInterstitial({
    adId: admobid.interstitial,
 
    autoShow: true
  });
}

